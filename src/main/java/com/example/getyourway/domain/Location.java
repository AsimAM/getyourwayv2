package com.example.getyourway.domain;

// USER INPUT
// LOCATION CLASS HOLDS LATITUDE AND LONGITUDE VALUES

public class Location {
    private String lat;
    private String lon;

    public Location(String lat, String lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }

    @Override
    public String toString() {
        return "Location{" +
                "lat='" + lat + '\'' +
                ", lon='" + lon + '\'' +
                '}';
    }
}
