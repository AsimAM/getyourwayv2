package com.example.getyourway.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


import java.util.List;

// **** Mapping JSON from API: http://api.openweathermap.org/data/2.5/forecast?lat=51.505432&lon=-20.023533&appid=04c53fd6e2611f4a1d8a78bdbec57b28

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherConditions {

    private String dt;
    private Main main;
    private List<Weather> weather;

    public WeatherConditions() {
    }

    public String getDt() {
        return dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }
}
