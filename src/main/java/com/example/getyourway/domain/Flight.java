package com.example.getyourway.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Flight {
    String dTime;
    String aTime;
    String fly_duration;
    String cityFrom;
    String flyFrom;         //IATA
    String cityTo;
    String flyTo;           //IATA
    double price;           //Euros
    String deep_link;
    @JsonProperty("route")
    List<AirportLocations> airportLocations;

    public Flight() {
    }

    public String getdTime() {
        return dTime;
    }

    public void setdTime(String dTime) {
        this.dTime = dTime;
    }

    public String getaTime() {
        return aTime;
    }

    public void setaTime(String aTime) {
        this.aTime = aTime;
    }

    public String getFly_duration() {
        return fly_duration;
    }

    public void setFly_duration(String fly_duration) {
        this.fly_duration = fly_duration;
    }

    public String getCityFrom() {
        return cityFrom;
    }

    public void setCityFrom(String cityFrom) {
        this.cityFrom = cityFrom;
    }

    public String getFlyFrom() {
        return flyFrom;
    }

    public void setFlyFrom(String flyFrom) {
        this.flyFrom = flyFrom;
    }

    public String getCityTo() {
        return cityTo;
    }

    public void setCityTo(String cityTo) {
        this.cityTo = cityTo;
    }

    public String getFlyTo() {
        return flyTo;
    }

    public void setFlyTo(String flyTo) {
        this.flyTo = flyTo;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDeep_link() {
        return deep_link;
    }

    public void setDeep_link(String deep_link) {
        this.deep_link = deep_link;
    }

    public List<AirportLocations> getAirportLocations() {
        return airportLocations;
    }

    public void setAirportLocations(List<AirportLocations> airportLocations) {
        this.airportLocations = airportLocations;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "dTime=" + dTime +
                ", aTime=" + aTime +
                ", fly_duration='" + fly_duration + '\'' +
                ", flyFrom='" + flyFrom + '\'' +
                ", flyTo='" + flyTo + '\'' +
                ", price=" + price +
                '}';
    }
}
