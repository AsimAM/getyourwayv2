package com.example.getyourway.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

// **** Mapping JSON from API: http://api.openweathermap.org/data/2.5/forecast?lat=51.505432&lon=-20.023533&appid=04c53fd6e2611f4a1d8a78bdbec57b28

@JsonIgnoreProperties(ignoreUnknown = true)
public class Weather {

    private String main;

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    @Override
    public String toString() {
        return "Weather{" +
                "main='" + main + '\'' +
                '}';
    }
}
