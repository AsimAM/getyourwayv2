package com.example.getyourway.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

// **** Mapping JSON from API: (Gets everything from "List") http://api.openweathermap.org/data/2.5/forecast?lat=51.505432&lon=-20.023533&appid=04c53fd6e2611f4a1d8a78bdbec57b28

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherResponse {

    // unneeded
    @JsonProperty("list")
    List<WeatherConditions> allWeather;

    public WeatherResponse() {
    }

    public List<WeatherConditions> getAllWeather() {
        return allWeather;
    }

    public void setAllWeather(List<WeatherConditions> allWeather) {
        this.allWeather = allWeather;
    }

}
