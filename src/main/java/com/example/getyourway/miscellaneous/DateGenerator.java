package com.example.getyourway.miscellaneous;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateGenerator {

    public static String setCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Date currentDate = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        return dateFormat.format(cal.getTime());
    }

    public static String setDateInTwoWeeks() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Date futureDate = new Date();
        Calendar futureCal = Calendar.getInstance();
        futureCal.setTime(futureDate);
        futureCal.add(Calendar.DATE, 14);
        return dateFormat.format(futureCal.getTime());

    }

}
