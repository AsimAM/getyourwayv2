package com.example.getyourway.controller;

import com.example.getyourway.service.AllAPIService;
import com.example.getyourway.payload.request.UserRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/auth")
public class AllAPIController {
    @Autowired
    private AllAPIService allApiService;

    @PostMapping("/alljourneys")
    public ResponseEntity<Object> getAllJourneyData(@Valid @RequestBody UserRequest userRequest) throws JsonProcessingException, JSONException, URISyntaxException {

        return allApiService.getAllJourneyData(userRequest);
    }
}

