package com.example.getyourway.controller;

import com.example.getyourway.payload.test.DrivingRoute;
import com.example.getyourway.payload.request.RouteRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.net.URISyntaxException;
import java.util.List;

import com.example.getyourway.service.RouteService;
import org.json.JSONException;

@RestController
@RequestMapping("/api/auth")
public class RouteController {

    @Autowired
    private RouteService routeService;

    @PostMapping("/route")
    public List<DrivingRoute> getRoutes(@RequestBody RouteRequest routeRequest) throws JsonProcessingException, URISyntaxException, JSONException {

        return routeService.getRoutes(routeRequest);
    }
}