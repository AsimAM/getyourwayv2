package com.example.getyourway.controller;

import com.example.getyourway.payload.test.Weather;
import com.example.getyourway.payload.request.WeatherRequest;
import com.example.getyourway.payload.test.Journeys;
import com.example.getyourway.service.WeatherService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;

// CONTROLLER WILL RECEIVE THE USER'S REQUEST AND SEND BACK THE DATA THEY HAVE ASKED FOR.
// IT TAKES IN THE LOCATION THE USER ENTERS

// Confirming they're authorized to make this request with /auth
@RestController
@RequestMapping("/api/auth")
public class WeatherController {

    // **** We instantiate a weather getter service
    @Autowired
    private WeatherService weatherService;

    // **** Make a post request which has the longitude and latitude of what the user entered.
    // **** Call the getWeather method, and return its data.
    @PostMapping("/currentLocationWeather")
    public Journeys getCurrentLocationWeather(@RequestBody WeatherRequest weatherRequest) throws JsonProcessingException, JSONException, URISyntaxException {

        return weatherService.getCurrentLocationWeather(weatherRequest);
    }

    @PostMapping("/airportWeather")
    public Weather getAirportWeather(@RequestBody WeatherRequest weatherRequest) throws JsonProcessingException, JSONException, URISyntaxException {

        return weatherService.getAirportWeather(weatherRequest);
    }
}