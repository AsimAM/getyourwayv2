package com.example.getyourway.controller;

import com.example.getyourway.payload.test.Flight;
import com.example.getyourway.payload.request.AirportRequest;
import com.example.getyourway.service.AllAirports;
import com.example.getyourway.service.FlightService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api/auth")
public class FlightController {

    @Autowired
    private FlightService flightService;

    @PostMapping("/airports")
    public AllAirports getAirports(@RequestBody AirportRequest airportRequest) throws JsonProcessingException, JSONException, URISyntaxException {

        return flightService.getAirports(airportRequest);
    }

    @PostMapping("/flights")
    public List<Flight> getFlights(@RequestBody AllAirports allAirports) throws JsonProcessingException, JSONException, URISyntaxException {

        return flightService.callFlightAPI(allAirports);
    }
}
