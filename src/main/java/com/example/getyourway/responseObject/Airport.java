package com.example.getyourway.responseObject;

public class Airport {

    String lat;
    String lon;

    public Airport() {
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    @Override
    public String toString() {
        return "Airport{" +
                "lat=" + lat +
                ", lon=" + lon +
                '}';
    }
}
