package com.example.getyourway.responseObject;

public class FlightLegCoordinatesForThatJourney {

    Airport departureAirport;
    Airport arrivalAirport;

    public FlightLegCoordinatesForThatJourney() {
    }

    public Airport getDepartureAirport() {
        return departureAirport;
    }

    public void setDepartureAirport(Airport departureAirport) {
        this.departureAirport = departureAirport;
    }

    public Airport getArrivalAirport() {
        return arrivalAirport;
    }

    public void setArrivalAirport(Airport arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }

    @Override
    public String toString() {
        return "FlightLegCoordinatesForThatJourney{" +
                "departureAirport=" + departureAirport +
                ", arrivalAirport=" + arrivalAirport +
                '}';
    }
}
