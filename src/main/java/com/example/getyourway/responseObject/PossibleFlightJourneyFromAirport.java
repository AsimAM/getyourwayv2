package com.example.getyourway.responseObject;

import java.util.List;

public class PossibleFlightJourneyFromAirport {

    int flightJourneyId;
    double flightPrice;
    String flightBookingLink;
    String duration;
    String departureAirportCode;
    String arrivalAirportCode;
    List<FlightOnThatJourney> flightsOnThatJourney;

    public PossibleFlightJourneyFromAirport() {
    }

    public String getDepartureAirportCode() {
        return departureAirportCode;
    }

    public void setDepartureAirportCode(String departureAirportCode) {
        this.departureAirportCode = departureAirportCode;
    }

    public String getArrivalAirportCode() {
        return arrivalAirportCode;
    }

    public void setArrivalAirportCode(String arrivalAirportCode) {
        this.arrivalAirportCode = arrivalAirportCode;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public double getFlightPrice() {
        return flightPrice;
    }

    public void setFlightPrice(double flightPrice) {
        this.flightPrice = flightPrice;
    }

    public String getFlightBookingLink() {
        return flightBookingLink;
    }

    public void setFlightBookingLink(String flightBookingLink) {
        this.flightBookingLink = flightBookingLink;
    }

    public int getFlightJourneyId() {
        return flightJourneyId;
    }

    public void setFlightJourneyId(int flightJourneyId) {
        this.flightJourneyId = flightJourneyId;
    }

    public List<FlightOnThatJourney> getFlightsOnThatJourney() {
        return flightsOnThatJourney;
    }

    public void setFlightsOnThatJourney(List<FlightOnThatJourney> flightsOnThatJourney) {
        this.flightsOnThatJourney = flightsOnThatJourney;
    }

    @Override
    public String toString() {
        return "PossibleFlightJourneyFromAirport{" +
                "flightJourneyId=" + flightJourneyId +
                '}';
    }
}
