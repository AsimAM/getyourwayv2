package com.example.getyourway.responseObject;

import java.util.List;

public class DrivingCoordinates {

    List<Double[]> coordinates;

    public DrivingCoordinates() {
    }

    public List<Double[]> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Double[]> coordinates) {
        this.coordinates = coordinates;
    }
}
