package com.example.getyourway.responseObject;

import java.util.List;

public class PossibleJourneysFromOneAirport {

    List<PossibleDrivingRoute> possibleDrivingRoutes;
    List<PossibleFlightJourneyFromAirport> allFlightJourneysFromThatAirport;
    WeatherCondition currentLocationWeather;
    PossibleJourneyCoordinatesTiedToThatAirport allJourneyCoordinatesTiedToThatAirport;

    public PossibleJourneysFromOneAirport() {
    }

    public List<PossibleDrivingRoute> getPossibleDrivingRoutes() {
        return possibleDrivingRoutes;
    }

    public void setPossibleDrivingRoutes(List<PossibleDrivingRoute> possibleDrivingRoutes) {
        this.possibleDrivingRoutes = possibleDrivingRoutes;
    }

    public List<PossibleFlightJourneyFromAirport> getAllFlightJourneysFromThatAirport() {
        return allFlightJourneysFromThatAirport;
    }

    public void setAllFlightJourneysFromThatAirport(List<PossibleFlightJourneyFromAirport> allFlightJourneysFromThatAirport) {
        this.allFlightJourneysFromThatAirport = allFlightJourneysFromThatAirport;
    }

    public WeatherCondition getCurrentLocationWeather() {
        return currentLocationWeather;
    }

    public void setCurrentLocationWeather(WeatherCondition currentLocationWeather) {
        this.currentLocationWeather = currentLocationWeather;
    }

    public PossibleJourneyCoordinatesTiedToThatAirport getAllJourneyCoordinatesTiedToThatAirport() {
        return allJourneyCoordinatesTiedToThatAirport;
    }

    public void setAllJourneyCoordinatesTiedToThatAirport(PossibleJourneyCoordinatesTiedToThatAirport allJourneyCoordinatesTiedToThatAirport) {
        this.allJourneyCoordinatesTiedToThatAirport = allJourneyCoordinatesTiedToThatAirport;
    }

    @Override
    public String toString() {
        return "PossibleJourneysFromOneAirport{" +
                ", allJourneyCoordinatesTiedToThatAirport=" + allJourneyCoordinatesTiedToThatAirport +
                '}';
    }
}
