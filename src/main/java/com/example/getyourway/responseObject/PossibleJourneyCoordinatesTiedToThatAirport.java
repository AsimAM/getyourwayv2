package com.example.getyourway.responseObject;

import java.util.List;

public class PossibleJourneyCoordinatesTiedToThatAirport {

    List<DrivingRouteCoordinatesToThatAirport> allDrivingRouteCoordinatesToThatAirport;
    List<FlightJourneyCoordinatesFromThatAirport> allFlightJourneyCoordinatesFromThatAirport;

    public PossibleJourneyCoordinatesTiedToThatAirport() {
    }

    public List<DrivingRouteCoordinatesToThatAirport> getAllDrivingRouteCoordinatesToThatAirport() {
        return allDrivingRouteCoordinatesToThatAirport;
    }

    public void setAllDrivingRouteCoordinatesToThatAirport(List<DrivingRouteCoordinatesToThatAirport> allDrivingRouteCoordinatesToThatAirport) {
        this.allDrivingRouteCoordinatesToThatAirport = allDrivingRouteCoordinatesToThatAirport;
    }

    public List<FlightJourneyCoordinatesFromThatAirport> getAllFlightJourneyCoordinatesFromThatAirport() {
        return allFlightJourneyCoordinatesFromThatAirport;
    }

    public void setAllFlightJourneyCoordinatesFromThatAirport(List<FlightJourneyCoordinatesFromThatAirport> allFlightJourneyCoordinatesFromThatAirport) {
        this.allFlightJourneyCoordinatesFromThatAirport = allFlightJourneyCoordinatesFromThatAirport;
    }

    @Override
    public String toString() {
        return "PossibleJourneyCoordinatesTiedToThatAirport{" +
                ", allFlightJourneyCoordinatesFromThatAirport=" + allFlightJourneyCoordinatesFromThatAirport +
                '}';
    }
}
