package com.example.getyourway.responseObject;

public class WeatherCondition {

    double temp;
    String condition;

    public WeatherCondition() {
    }

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }
}
