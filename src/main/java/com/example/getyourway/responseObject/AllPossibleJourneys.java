package com.example.getyourway.responseObject;

import java.util.List;

public class AllPossibleJourneys {

    List<PossibleJourneysFromOneAirport> possibleJourneysFromEachAirport;

    public AllPossibleJourneys() {
    }

    public List<PossibleJourneysFromOneAirport> getPossibleJourneysFromEachAirport() {
        return possibleJourneysFromEachAirport;
    }

    public void setPossibleJourneysFromEachAirport(List<PossibleJourneysFromOneAirport> possibleJourneysFromEachAirport) {
        this.possibleJourneysFromEachAirport = possibleJourneysFromEachAirport;
    }

    @Override
    public String toString() {
        return "AllPossibleJourneys{" +
                "possibleJourneysFromEachAirport=" + possibleJourneysFromEachAirport +
                '}';
    }
}
