package com.example.getyourway.responseObject;

import java.util.List;

public class DrivingRouteCoordinatesToThatAirport {

    int drivingRouteId;
    List<Double[]> coordinates;

    public DrivingRouteCoordinatesToThatAirport() {
    }

    public int getDrivingRouteId() {
        return drivingRouteId;
    }

    public void setDrivingRouteId(int drivingRouteId) {
        this.drivingRouteId = drivingRouteId;
    }

    public List<Double[]> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Double[]> coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public String toString() {
        return "DrivingRouteCoordinatesToThatAirport{" +
                "drivingRouteId=" + drivingRouteId +
                ", coordinates=" + coordinates +
                '}';
    }
}
