package com.example.getyourway.responseObject;

import java.util.List;

public class FlightJourneyCoordinatesFromThatAirport {

    int flightJourneyId;
    List<FlightLegCoordinatesForThatJourney> flightLegCoordinatesForThatJourneyList;

    public FlightJourneyCoordinatesFromThatAirport() {
    }

    public int getFlightJourneyId() {
        return flightJourneyId;
    }

    public void setFlightJourneyId(int flightJourneyId) {
        this.flightJourneyId = flightJourneyId;
    }

    public List<FlightLegCoordinatesForThatJourney> getFlightLegCoordinatesForThatJourneyList() {
        return flightLegCoordinatesForThatJourneyList;
    }

    public void setFlightLegCoordinatesForThatJourneyList(List<FlightLegCoordinatesForThatJourney> flightLegCoordinatesForThatJourneyList) {
        this.flightLegCoordinatesForThatJourneyList = flightLegCoordinatesForThatJourneyList;
    }

    @Override
    public String toString() {
        return "FlightJourneyCoordinatesFromThatAirport{" +
                "flightJourneyId=" + flightJourneyId +
                ", flightLegCoordinatesForThatJourneyList=" + flightLegCoordinatesForThatJourneyList +
                '}';
    }
}
