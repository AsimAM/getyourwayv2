package com.example.getyourway.responseObject;

public class FlightOnThatJourney {

    WeatherCondition departureWeather;
    WeatherCondition arrivalWeather;
    String departureCityName;
    String departureCityCode;
    String arrivalCityName;
    String arrivalCityCode;
    String departureTime;
    String arrivalTime;


    public FlightOnThatJourney() {
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public WeatherCondition getDepartureWeather() {
        return departureWeather;
    }

    public void setDepartureWeather(WeatherCondition departureWeather) {
        this.departureWeather = departureWeather;
    }

    public WeatherCondition getArrivalWeather() {
        return arrivalWeather;
    }

    public void setArrivalWeather(WeatherCondition arrivalWeather) {
        this.arrivalWeather = arrivalWeather;
    }

    public String getDepartureCityName() {
        return departureCityName;
    }

    public void setDepartureCityName(String departureCityName) {
        this.departureCityName = departureCityName;
    }

    public String getDepartureCityCode() {
        return departureCityCode;
    }

    public void setDepartureCityCode(String departureCityCode) {
        this.departureCityCode = departureCityCode;
    }

    public String getArrivalCityName() {
        return arrivalCityName;
    }

    public void setArrivalCityName(String arrivalCityName) {
        this.arrivalCityName = arrivalCityName;
    }

    public String getArrivalCityCode() {
        return arrivalCityCode;
    }

    public void setArrivalCityCode(String arrivalCityCode) {
        this.arrivalCityCode = arrivalCityCode;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }


    @Override
    public String toString() {
        return "FlightOnThatJourney{" +
                "departureCityCode='" + departureCityCode + '\'' +
                ", arrivalCityCode='" + arrivalCityCode + '\'' +
                '}';
    }
}
