package com.example.getyourway.responseObject;

import java.util.List;

public class PossibleDrivingRoute {

    int drivingRouteId;
    double duration;
    double distance;
    List<Step> steps;

    public PossibleDrivingRoute() {
    }

    public int getDrivingRouteId() {
        return drivingRouteId;
    }

    public void setDrivingRouteId(int drivingRouteId) {
        this.drivingRouteId = drivingRouteId;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    @Override
    public String toString() {
        return "PossibleDrivingRoute{" +
                "drivingRouteId=" + drivingRouteId +
                ", duration=" + duration +
                ", distance=" + distance +
                '}';
    }
}
