package com.example.getyourway.service;

import com.example.getyourway.payload.request.AirportRequest;
import com.example.getyourway.payload.test.Airport;
import com.example.getyourway.payload.test.Flight;
import com.example.getyourway.miscellaneous.DateGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class FlightService {

    @Autowired
    private RestTemplate restTemplate;

    public AllAirports getAirports(AirportRequest airportRequest) throws URISyntaxException, JSONException, JsonProcessingException {
        List<Airport> airportEndList = new ArrayList<>();
        List<Airport> airportStartList = new ArrayList<>();

        String link = "https://www.air-port-codes.com/api/v1/multi?term=";
        String keyType = "APC-Auth";
        String keyValue = "b548debedc";
        String secretKeyType  = "APC-Auth-Secret";
        String secretKeyValue = "87c8f150d593268";

        URI startURI = new URI(link + airportRequest.getStartAirport() + "&limit=2");
        URI endURI = new URI(link + airportRequest.getEndAirport() + "&limit=2");

        HttpHeaders headers = new HttpHeaders();
        headers.set(keyType, keyValue);
        headers.set(secretKeyType, secretKeyValue);
        HttpEntity<String> request = new HttpEntity<String>(headers);

        ResponseEntity<String> startResponse = restTemplate.exchange(startURI, HttpMethod.GET, request, String.class);
        JSONObject startWrapper = new JSONObject(startResponse.getBody());
        JSONArray startArray = startWrapper.getJSONArray("airports");
        airportStartList = new ObjectMapper().readValue(startArray.toString(), new TypeReference<List<Airport>>() {});

        ResponseEntity<String> endResponse = restTemplate.exchange(endURI, HttpMethod.GET, request, String.class);
        JSONObject endWrapper = new JSONObject(endResponse.getBody());
        JSONArray endArray = endWrapper.getJSONArray("airports");
        airportEndList = new ObjectMapper().readValue(endArray.toString(), new TypeReference<List<Airport>>() {});

        List<Airport> mergedList = Stream.concat(airportStartList.stream(), airportEndList.stream())
                .collect(Collectors.toList());

        AllAirports allAirports = new AllAirports();
        allAirports.setArrivalAirports(airportEndList);
        allAirports.setDepartureAirports(airportStartList);

        return allAirports;
    }

    public List<Flight> callFlightAPI(AllAirports allAirports) throws JSONException, JsonProcessingException, URISyntaxException {

        String startIATA = allAirports.getDepartureAirports().get(0).getAirportCodeName();
        String endIATA = allAirports.getArrivalAirports().get(0).getAirportCodeName();

        String stringDateFrom = DateGenerator.setCurrentDate();
        String stringDateTo = DateGenerator.setDateInTwoWeeks();

        String link = "https://api.skypicker.com/flights?";
        String flyFrom = "flyFrom=" + startIATA;
        String flyTo = "&to=" + endIATA;
        String dateFrom = "&dateFrom=" + stringDateFrom;
        String dateTo = "&dateTo=" + stringDateTo;
        String partner = "&partner=picky";
        String v = "&v=3";
        String limit = "&limit=10";

        URI flightUri = new URI(link + flyFrom + flyTo + dateFrom + dateTo + limit + partner + v);

        List<Flight> flightList = new ArrayList<>();
        String flightResponse = restTemplate.getForObject(flightUri, String.class);
        JSONObject flightWrapper = new JSONObject(flightResponse);
        JSONArray allFlights = flightWrapper.getJSONArray("data");
        flightList = new ObjectMapper().readValue(allFlights.toString(), new TypeReference<List<Flight>>() {});

        return flightList;
    }
}
