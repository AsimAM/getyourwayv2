package com.example.getyourway.service;

import com.example.getyourway.payload.test.Weather;
import com.example.getyourway.payload.request.WeatherRequest;
import com.example.getyourway.domain.*;
import com.example.getyourway.payload.test.Journeys;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

// SERVICE DOES BUSINESS LOGIC
// The getWeather() method returns ONE weather, namely today's weather! It comes from a list of weather, showing the forecast.

@Service
public class WeatherService {

    @Autowired
    RestTemplate restTemplate;

    public Journeys getCurrentLocationWeather(WeatherRequest weatherRequest) throws JSONException, JsonProcessingException, URISyntaxException {
//
        // ** Transform request object into request data.
        String startLat = weatherRequest.getLat();
        String startLon = weatherRequest.getLon();

        // **** Creating the URI
        String key = "bbf3a86e35c9d3763fe5782119b7f9a5"; // key is hardcoded
        String lat = startLat; // latitude is pulled in
        String lon = startLon; // longitude is pulled in
        URI reqURI = new URI("http://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon + "&appid=" + key);

        // **** Getting the data from the API. Getting one part of the array to get ONE weather.
        String weatherResponse = restTemplate.getForObject(reqURI, String.class);

        // **** Take the response and turn it into a JSON object
        JSONObject weatherJSONObject = new JSONObject(weatherResponse);

        Journeys journeys = new ObjectMapper().readValue(weatherJSONObject.toString(), new TypeReference<Journeys>() {});

        return journeys;
    }

    public Weather getAirportWeather(WeatherRequest weatherRequest) throws JSONException, JsonProcessingException, URISyntaxException {
//
        // ** Transform request object into request data.
        String startLat = weatherRequest.getLat();
        String startLon = weatherRequest.getLon();

        // **** Creating the URI
        String key = "bbf3a86e35c9d3763fe5782119b7f9a5"; // key is hardcoded
        String lat = startLat; // latitude is pulled in
        String lon = startLon; // longitude is pulled in
        URI reqURI = new URI("http://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon + "&appid=" + key);

        // **** Getting the data from the API. Getting one part of the array to get ONE weather.
        String weatherResponse = restTemplate.getForObject(reqURI, String.class);

        // **** Take the response and turn it into a JSON object
        JSONObject weatherJSONObject = new JSONObject(weatherResponse);

        Weather weather = new ObjectMapper().readValue(weatherJSONObject.toString(), new TypeReference<Weather>() {});

        return weather;
    }

}
