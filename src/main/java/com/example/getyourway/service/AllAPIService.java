package com.example.getyourway.service;

import com.example.getyourway.controller.FlightController;
import com.example.getyourway.controller.RouteController;
import com.example.getyourway.controller.WeatherController;
import com.example.getyourway.payload.request.RouteRequest;
import com.example.getyourway.payload.request.UserRequest;
import com.example.getyourway.payload.request.WeatherRequest;
import com.example.getyourway.payload.test.DrivingRoute;
import com.example.getyourway.payload.test.Flight;
import com.example.getyourway.payload.test.Journeys;
import com.example.getyourway.payload.test.Weather;
import com.example.getyourway.service.AllAirports;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URISyntaxException;
import java.util.List;

@Service
public class AllAPIService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private FlightController flightController;

    @Autowired
    private RouteController routeController;

    @Autowired
    private WeatherController weatherController;

    @Bean
    public RestTemplate restTemplateForAPI(RestTemplateBuilder builder) {
        return builder.build();
    }

    public ResponseEntity<Object> getAllJourneyData(UserRequest userRequest) throws URISyntaxException, JSONException, JsonProcessingException {

        WeatherRequest weatherRequest = new WeatherRequest(userRequest.getCurrentLat(), userRequest.getCurrentLon());

        Journeys journeys = weatherController.getCurrentLocationWeather(weatherRequest);

        AllAirports allAirports = flightController.getAirports(userRequest.getAirportRequest());

        journeys.setDepartureCity(allAirports.getDepartureAirports().get(0));

        List<Flight> flights = flightController.getFlights(allAirports);

        for (int i = 0; i < flights.size(); i++) {
            if(i == 0) {
                WeatherRequest departureWeatherRequest = new WeatherRequest(flights.get(i).getFlightLegs().get(i).getDepartureLat(), flights.get(i).getFlightLegs().get(i).getDepartureLon());
                Weather departureWeather = weatherController.getAirportWeather(departureWeatherRequest);

                for (Flight flight : flights) {
                    flight.getFlightLegs().get(0).setDepartureWeather(departureWeather);
                }
            }

            RouteRequest routeRequest = new RouteRequest();
            String drivingArrivalLat = flights.get(i).getFlightLegs().get(0).getDepartureLat();
            String drivingArrivalLon = flights.get(i).getFlightLegs().get(0).getDepartureLon();
            routeRequest.setEndLat(drivingArrivalLat);
            routeRequest.setEndLon(drivingArrivalLon);
            routeRequest.setStartLat(userRequest.getCurrentLat());
            routeRequest.setStartLon(userRequest.getCurrentLon());

            List<DrivingRoute> drivingRoutes = routeController.getRoutes(routeRequest);
            flights.get(i).setDrivingRoutes(drivingRoutes);

            for (int k = 0; k < drivingRoutes.size(); k++) {
                drivingRoutes.get(k).setDrivingRouteId(k + 1);
            }

            flights.get(i).setFlightId(i + 1);

            for (int j = 0; j < flights.get(i).getFlightLegs().size(); j++) {

                WeatherRequest arrivalWeatherRequest = new WeatherRequest(flights.get(i).getFlightLegs().get(j).getDepartureLat(), flights.get(i).getFlightLegs().get(j).getDepartureLon());
                Weather arrivalWeather = weatherController.getAirportWeather(arrivalWeatherRequest);

                flights.get(i).getFlightLegs().get(j).setArrivalWeather(arrivalWeather);

                if (j < flights.get(i).getFlightLegs().size() - 1) {
                    flights.get(i).getFlightLegs().get(j + 1).setDepartureWeather(arrivalWeather);
                }
            }
        }
        journeys.getDepartureCity().setFlights(flights);

        return ResponseEntity.ok().body(journeys);
    }
}