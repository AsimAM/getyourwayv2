package com.example.getyourway.service;

import com.example.getyourway.payload.test.Airport;

import java.util.ArrayList;
import java.util.List;

public class AllAirports {

    List<Airport> departureAirports = new ArrayList<>();
    List<Airport> arrivalAirports = new ArrayList<>();

    public AllAirports() {
    }

    public List<Airport> getDepartureAirports() {
        return departureAirports;
    }

    public void setDepartureAirports(List<Airport> departureAirports) {
        this.departureAirports = departureAirports;
    }

    public List<Airport> getArrivalAirports() {
        return arrivalAirports;
    }

    public void setArrivalAirports(List<Airport> arrivalAirports) {
        this.arrivalAirports = arrivalAirports;
    }

    @Override
    public String toString() {
        return "AllAirports{" +
                "departureAirports=" + departureAirports +
                ", arrivalAirports=" + arrivalAirports +
                '}';
    }
}
