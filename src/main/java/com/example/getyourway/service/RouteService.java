package com.example.getyourway.service;

import com.example.getyourway.payload.test.DrivingRoute;
import com.example.getyourway.payload.request.RouteRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Service
public class RouteService {

    @Autowired
    private RestTemplate restTemplate;

    public List<DrivingRoute> getRoutes(RouteRequest routeRequest) throws JsonProcessingException, URISyntaxException, JSONException {

        List<DrivingRoute> routesList;

        String startLat = routeRequest.getStartLat();
        String startLon = routeRequest.getStartLon();
        String endLat = routeRequest.getEndLat();
        String endLon = routeRequest.getEndLon();

        String apiURL = "https://api.mapbox.com/directions/v5/mapbox/driving/";
        String apiOptions = "?alternatives=true&geometries=geojson&steps=true&access_token=";
        String apiKey = "pk.eyJ1IjoiYXNkaGFpc291aCIsImEiOiJja2dlM3dndHowcDE3MnFvN2JoaTNodW5nIn0.cadcTKoExEB-oGmZud3qVQ";

        URI requestURL = new URI(apiURL + startLon + "," + startLat + ";" + endLon + "," + endLat + apiOptions + apiKey);

        String routeAPIResponse = restTemplate.getForObject(requestURL, String.class);

        JSONObject routeResponseJsonObject = new JSONObject(routeAPIResponse);

        JSONArray allRoutes = routeResponseJsonObject.getJSONArray("routes");

        routesList = new ObjectMapper().readValue(allRoutes.toString(), new TypeReference<List<DrivingRoute>>() {
        });

        return routesList;
    }

}