package com.example.getyourway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GetyourwayApplication {

	public static void main(String[] args) {
		SpringApplication.run(GetyourwayApplication.class, args);
	}
}
