package com.example.getyourway.payload.request;

public class AirportRequest {

    private String startAirport;
    private String endAirport;

    public AirportRequest(String startAirport, String endAirport) {
        this.startAirport = startAirport;
        this.endAirport = endAirport;
    }

    public String getStartAirport() {
        return startAirport;
    }

    public void setStartAirport(String startAirport) {
        this.startAirport = startAirport;
    }

    public String getEndAirport() {
        return endAirport;
    }

    public void setEndAirport(String endAirport) {
        this.endAirport = endAirport;
    }

    @Override
    public String toString() {
        return "AirportRequest{" +
                "startAirport='" + startAirport + '\'' +
                ", endAirport='" + endAirport + '\'' +
                '}';
    }
}
