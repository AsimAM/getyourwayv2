package com.example.getyourway.payload.request;

// USER INPUT

public class WeatherRequest {

    private String lat;
    private String lon;

    public WeatherRequest() {
    }

    public WeatherRequest(String lat, String lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    @Override
    public String toString() {
        return "WeatherRequest{" +
                "lat='" + lat + '\'' +
                ", lon='" + lon + '\'' +
                '}';
    }
}
