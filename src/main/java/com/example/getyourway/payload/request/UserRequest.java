package com.example.getyourway.payload.request;

public class UserRequest {

    private AirportRequest airportRequest;
    private String currentLat;
    private String currentLon;

    public UserRequest(AirportRequest airportRequest, String currentLat, String currentLon) {
        this.airportRequest = airportRequest;
        this.currentLat = currentLat;
        this.currentLon = currentLon;
    }

    public AirportRequest getAirportRequest() {
        return airportRequest;
    }

    public void setAirportRequest(AirportRequest airportRequest) {
        this.airportRequest = airportRequest;
    }

    public String getCurrentLat() {
        return currentLat;
    }

    public void setCurrentLat(String currentLat) {
        this.currentLat = currentLat;
    }

    public String getCurrentLon() {
        return currentLon;
    }

    public void setCurrentLon(String currentLon) {
        this.currentLon = currentLon;
    }
}
