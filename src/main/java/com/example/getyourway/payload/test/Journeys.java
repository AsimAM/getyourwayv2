package com.example.getyourway.payload.test;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Journeys {

    Weather weatherAtStartPoint = new Weather();
    Airport departureCity;

    public Journeys() {
    }


//    @JsonProperty("airports")
//    private void getDepartureAirports(List<Map<String, String>> airports) {
//
//        for (Map<String, String> airport: airports) {
//            Airport departureAirport = new Airport();
//            departureAirport.setAirportCodeName(airport.get("iata"));
//            departureAirport.setAirportName(airport.get("city"));
//            this.departureAirports.add(departureAirport);
//        }
//    }

    //    @SuppressWarnings("unchecked")
    @JsonProperty("weather")
    private void getWeatherConditionAtStartPoint(List<Map<String, String>> weatherResponse) {
        this.weatherAtStartPoint.setCondition(weatherResponse.get(0).get("main"));
    }

    @JsonProperty("main")
    private void getWeatherTempAtStartPoint(Map<String, String> weatherResponse) {
        this.weatherAtStartPoint.setTemp((String) weatherResponse.get("temp"));
    }

    public void setWeatherAtStartPoint(Weather weatherAtStartPoint) {
        this.weatherAtStartPoint = weatherAtStartPoint;
    }

    public void setDepartureCity(Airport departureCity) {
        this.departureCity = departureCity;
    }

    public Airport getDepartureCity() {
        return departureCity;
    }

    public Weather getWeatherAtStartPoint() {
        return weatherAtStartPoint;
    }

    @Override
    public String toString() {
        return "Journeys{" +
                "weatherAtStartPoint=" + weatherAtStartPoint +
                '}';
    }

    // weatherAtStartPoint
        // temp
        // condition
    // departureAirports
        // airportName
        // airportCodeName
        // flights
            // flightId
            // modeOfTransport = "flying"
            // departureAirportName
            // departureAirportCodeName
            // departureTime
            // arrivalAirportName
            // arrivalAirportCodeName
            // arrivalTime
            // flightPrice
            // flightBookingLink
            // flightDuration
            // flightLegs
                // departureAirportName
                // departureAirportCodeName
                // departureTime
                // departureWeather
                    // arrivalTemp
                    // arrivalCondition
                // arrivalAirportName
                // arrivalAirportCodeName
                // arrivalTime
                // arrivalWeather
                    // arrivalTemp
                    // arrivalCondition
                // flightDuration
                // departureLat
                // departureLon
                // arrivalLat
                // arrivalLon
        // drivingRoutes
            // modeOfTransport = "driving"
            // drivingDuration
            // drivingDistance
            // steps
                // instruction
                // duration
                // distance
                // coordinates
}