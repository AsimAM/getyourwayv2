package com.example.getyourway.payload.test;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DrivingRoute {

    int drivingRouteId;
    String modeOfTransport = "driving";

    @JsonProperty("duration")
    String drivingDuration;

    @JsonProperty("distance")
    String drivingDistance;

    List<Step> steps = new ArrayList<>();

    public DrivingRoute() {
    }

    @JsonProperty("legs")
    public void setStepsInDrivingRoute(List<Map<String, Object>> legs) {
        List<Map<String, Object>> stepsInRoute = (List<Map<String, Object>>) legs.get(0).get("steps");

        for (Map<String, Object> stepInRoute: stepsInRoute) {
            Step step = new Step();

            step.setDistance((String) stepInRoute.get("distance").toString());
            step.setDuration((String) stepInRoute.get("duration").toString());

            Map<String, String> maneuver = (LinkedHashMap<String, String>) stepInRoute.get("maneuver");
            step.setInstruction(maneuver.get("instruction"));

            Map<String, List<Object>> geometry = (Map<String, List<Object>>) stepInRoute.get("geometry");
            step.setCoordinates(geometry.get("coordinates"));

            this.steps.add(step);
        }
    }

    public int getDrivingRouteId() {
        return drivingRouteId;
    }

    public void setDrivingRouteId(int drivingRouteId) {
        this.drivingRouteId = drivingRouteId;
    }

    public String getModeOfTransport() {
        return modeOfTransport;
    }

    public void setModeOfTransport(String modeOfTransport) {
        this.modeOfTransport = modeOfTransport;
    }

    public String getDrivingDuration() {
        return drivingDuration;
    }

    public void setDrivingDuration(String drivingDuration) {
        this.drivingDuration = drivingDuration;
    }

    public String getDrivingDistance() {
        return drivingDistance;
    }

    public void setDrivingDistance(String drivingDistance) {
        this.drivingDistance = drivingDistance;
    }

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    @Override
    public String toString() {
        return "DrivingRoute{" +
                "modeOfTransport='" + modeOfTransport + '\'' +
                ", drivingDuration='" + drivingDuration + '\'' +
                ", drivingDistance='" + drivingDistance + '\'' +
                ", steps=" + steps +
                '}';
    }
}
