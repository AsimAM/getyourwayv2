package com.example.getyourway.payload.test;

import java.util.List;

public class Step {

    String instruction;
    String duration;
    String distance;
    List<Object> coordinates;

    public Step() {
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public List<Object> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Object> coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public String toString() {
        return "Step{" +
                "instruction='" + instruction + '\'' +
                ", duration='" + duration + '\'' +
                ", distance='" + distance + '\'' +
                ", coordinates=" + coordinates +
                '}';
    }
}
