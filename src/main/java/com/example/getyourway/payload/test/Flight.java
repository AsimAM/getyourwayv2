package com.example.getyourway.payload.test;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Flight {

     int flightId;
     String modeOfTransport = "flying";
     List<DrivingRoute> drivingRoutes = new ArrayList<>();

     @JsonProperty("cityFrom")
     String departureAirportName;

     @JsonProperty("flyFrom")
     String departureAirportCodeName;

     @JsonProperty("dTime")
     String departureTime;

     @JsonProperty("cityTo")
     String arrivalAirportName;

     @JsonProperty("flyTo")
     String arrivalAirportCodeName;

     @JsonProperty("aTime")
     String arrivalTime;

     @JsonProperty("price")
     String flightPrice;

     @JsonProperty("deep_link")
     String flightBookingLink;

     @JsonProperty("fly_duration")
     String flightDuration;

     List<FlightLeg> flightLegs = new ArrayList<>();

    public Flight() {
    }

    @JsonProperty("route")
    public void setFlightLegData(List<Map<String, String>> legs) {
        for (Map<String, String> leg: legs) {
            FlightLeg flightLeg = new FlightLeg();
            flightLeg.setArrivalAirportCodeName(leg.get("cityCodeTo"));
            flightLeg.setArrivalAirportName(leg.get("cityTo"));
            flightLeg.setArrivalTime(leg.get("aTime"));
            flightLeg.setDepartureAirportCodeName(leg.get("cityCodeFrom"));
            flightLeg.setDepartureAirportName(leg.get("cityFrom"));
            flightLeg.setDepartureTime(leg.get("dTime"));
            flightLeg.setDepartureLat(leg.get("latFrom"));
            flightLeg.setDepartureLon(leg.get("lngFrom"));
            flightLeg.setArrivalLat(leg.get("latTo"));
            flightLeg.setArrivalLon(leg.get("lngTo"));
            flightLegs.add(flightLeg);
        }
    }

    public List<DrivingRoute> getDrivingRoutes() {
        return drivingRoutes;
    }

    public void setDrivingRoutes(List<DrivingRoute> drivingRoutes) {
        this.drivingRoutes = drivingRoutes;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public String getModeOfTransport() {
        return modeOfTransport;
    }

    public void setModeOfTransport(String modeOfTransport) {
        this.modeOfTransport = modeOfTransport;
    }

    public String getDepartureAirportName() {
        return departureAirportName;
    }

    public void setDepartureAirportName(String departureAirportName) {
        this.departureAirportName = departureAirportName;
    }

    public String getDepartureAirportCodeName() {
        return departureAirportCodeName;
    }

    public void setDepartureAirportCodeName(String departureAirportCodeName) {
        this.departureAirportCodeName = departureAirportCodeName;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getArrivalAirportName() {
        return arrivalAirportName;
    }

    public void setArrivalAirportName(String arrivalAirportName) {
        this.arrivalAirportName = arrivalAirportName;
    }

    public String getArrivalAirportCodeName() {
        return arrivalAirportCodeName;
    }

    public void setArrivalAirportCodeName(String arrivalAirportCodeName) {
        this.arrivalAirportCodeName = arrivalAirportCodeName;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getFlightPrice() {
        return flightPrice;
    }

    public void setFlightPrice(String flightPrice) {
        this.flightPrice = flightPrice;
    }

    public String getFlightBookingLink() {
        return flightBookingLink;
    }

    public void setFlightBookingLink(String flightBookingLink) {
        this.flightBookingLink = flightBookingLink;
    }

    public String getFlightDuration() {
        return flightDuration;
    }

    public void setFlightDuration(String flightDuration) {
        this.flightDuration = flightDuration;
    }

    public List<FlightLeg> getFlightLegs() {
        return flightLegs;
    }

    public void setFlightLegs(List<FlightLeg> flightLegs) {
        this.flightLegs = flightLegs;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "flightId=" + flightId +
                ", modeOfTransport='" + modeOfTransport + '\'' +
                ", drivingRoutes=" + drivingRoutes +
                ", departureAirportName='" + departureAirportName + '\'' +
                ", departureAirportCodeName='" + departureAirportCodeName + '\'' +
                ", departureTime='" + departureTime + '\'' +
                ", arrivalAirportName='" + arrivalAirportName + '\'' +
                ", arrivalAirportCodeName='" + arrivalAirportCodeName + '\'' +
                ", arrivalTime='" + arrivalTime + '\'' +
                ", flightPrice='" + flightPrice + '\'' +
                ", flightBookingLink='" + flightBookingLink + '\'' +
                ", flightDuration='" + flightDuration + '\'' +
                ", flightLegs=" + flightLegs +
                '}';
    }
}
