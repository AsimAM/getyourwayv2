package com.example.getyourway.payload.test;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Airport {

    @JsonProperty("city")
    String airportName;

    @JsonProperty("iata")
    String airportCodeName;

    List<Flight> flights;


    public Airport() {
    }

    public String getAirportName() {
        return airportName;
    }

    public String getAirportCodeName() {
        return airportCodeName;
    }

    public List<Flight> getFlights() {
        return flights;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public void setAirportCodeName(String airportCodeName) {
        this.airportCodeName = airportCodeName;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }

    @Override
    public String toString() {
        return "Airport{" +
                "airportName='" + airportName + '\'' +
                ", airportCodeName='" + airportCodeName + '\'' +
                ", flights=" + flights +
                '}';
    }
}
