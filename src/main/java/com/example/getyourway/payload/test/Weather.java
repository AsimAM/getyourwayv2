package com.example.getyourway.payload.test;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Weather {

    String condition;
    String temp;

    public Weather() {
    }

    @JsonProperty("weather")
    private void getWeatherCondition(List<Map<String, String>> weatherResponse) {
        this.setCondition(weatherResponse.get(0).get("main"));
    }

    @JsonProperty("main")
    private void getWeatherTemp(Map<String, String> weatherResponse) {
        this.setTemp((String) weatherResponse.get("temp"));
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    @Override
    public String toString() {
        return "Weather{" +
                "condition='" + condition + '\'' +
                ", temp='" + temp + '\'' +
                '}';
    }
}
