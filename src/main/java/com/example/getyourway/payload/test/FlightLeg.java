package com.example.getyourway.payload.test;

public class FlightLeg {

    String departureAirportName;
    String departureAirportCodeName;
    String departureTime;
    Weather departureWeather;
    String arrivalAirportName;
    String arrivalAirportCodeName;
    String arrivalTime;
    Weather arrivalWeather;
    String departureLat;
    String departureLon;
    String arrivalLat;
    String arrivalLon;

    public FlightLeg() {
    }

    public String getDepartureAirportName() {
        return departureAirportName;
    }

    public void setDepartureAirportName(String departureAirportName) {
        this.departureAirportName = departureAirportName;
    }

    public String getDepartureAirportCodeName() {
        return departureAirportCodeName;
    }

    public void setDepartureAirportCodeName(String departureAirportCodeName) {
        this.departureAirportCodeName = departureAirportCodeName;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public Weather getDepartureWeather() {
        return departureWeather;
    }

    public void setDepartureWeather(Weather departureWeather) {
        this.departureWeather = departureWeather;
    }

    public String getArrivalAirportName() {
        return arrivalAirportName;
    }

    public void setArrivalAirportName(String arrivalAirportName) {
        this.arrivalAirportName = arrivalAirportName;
    }

    public String getArrivalAirportCodeName() {
        return arrivalAirportCodeName;
    }

    public void setArrivalAirportCodeName(String arrivalAirportCodeName) {
        this.arrivalAirportCodeName = arrivalAirportCodeName;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Weather getArrivalWeather() {
        return arrivalWeather;
    }

    public void setArrivalWeather(Weather arrivalWeather) {
        this.arrivalWeather = arrivalWeather;
    }

    public String getDepartureLat() {
        return departureLat;
    }

    public void setDepartureLat(String departureLat) {
        this.departureLat = departureLat;
    }

    public String getDepartureLon() {
        return departureLon;
    }

    public void setDepartureLon(String departureLon) {
        this.departureLon = departureLon;
    }

    public String getArrivalLat() {
        return arrivalLat;
    }

    public void setArrivalLat(String arrivalLat) {
        this.arrivalLat = arrivalLat;
    }

    public String getArrivalLon() {
        return arrivalLon;
    }

    public void setArrivalLon(String arrivalLon) {
        this.arrivalLon = arrivalLon;
    }

    @Override
    public String toString() {
        return "FlightLeg{" +
                "departureAirportName='" + departureAirportName + '\'' +
                ", departureAirportCodeName='" + departureAirportCodeName + '\'' +
                ", departureTime='" + departureTime + '\'' +
                ", departureWeather=" + departureWeather +
                ", arrivalAirportName='" + arrivalAirportName + '\'' +
                ", arrivalAirportCodeName='" + arrivalAirportCodeName + '\'' +
                ", arrivalTime='" + arrivalTime + '\'' +
                ", arrivalWeather=" + arrivalWeather +
                ", departureLat='" + departureLat + '\'' +
                ", departureLon='" + departureLon + '\'' +
                ", arrivalLat='" + arrivalLat + '\'' +
                ", arrivalLon='" + arrivalLon + '\'' +
                '}';
    }
}
