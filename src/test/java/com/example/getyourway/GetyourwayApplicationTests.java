package com.example.getyourway;

import com.example.getyourway.controller.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GetyourwayApplicationTests {

	@Autowired
	private AuthController authController;

	@Autowired
	private FlightController flightController;

//	@Autowired
//	private RouteController routeController;

	@Autowired
	private TestController testController;

	@Autowired
	private WeatherController weatherController;

	@Test
	void contextLoads() {
		assertThat(authController).isNotNull();
		assertThat(flightController).isNotNull();
//		assertThat(routeController).isNotNull();
		assertThat(testController).isNotNull();
		assertThat(weatherController).isNotNull();

	}
}
