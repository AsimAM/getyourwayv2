//package com.example.getyourway.controller;
//
//import com.example.getyourway.payload.request.WeatherRequest;
//import com.example.getyourway.domain.WeatherConditions;
//import com.example.getyourway.service.WeatherService;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//
//import static org.mockito.Mockito.when;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
//
//@ExtendWith(SpringExtension.class)
//public class WeatherControllerTest {
//
//    private MockMvc mockMvc;
//    @InjectMocks
//    private WeatherController weatherController;
//
//
//    @Mock
//    private WeatherService weatherService;
//
//
//    @BeforeEach
//    public void setup() throws Exception {
//        mockMvc = MockMvcBuilders.standaloneSetup(weatherController).build();
//    }
//
//    @Test
//    public void requestGetWeatherShouldReturnJSON() throws Exception {
//
//        // ARRANGE - this is doing the postman mock / postman's job
//        WeatherRequest mockWeatherRequest = new WeatherRequest("9", "9");
//        WeatherConditions weather = new WeatherConditions();
//
//
//
//        // response entity = type of object used to deal with JSONS, hence ResponseEntity, we want the body
//        // include the weather list,
//        // you can return a .ok.build because it builds it so the status is ok, but then it doesnt
//        // populate, SO IF YOU WANT TO PROBLEM SOLVE USE BUILD AS THEN YOU ARE NOT REPLYING ON JSON.
//        when(weatherService.getWeather(mockWeatherRequest)).thenReturn(weather);
//
//        //ACT  && ASSERT
//        // 54 - act , testing what happens when you make a post request
//        // 55 - json back is the assert
//        this.mockMvc.perform(post("/weather") // this will go to 49 and get the response that is mocked
//            .contentType("application/json"));
//
//
//
//
//    }
//
//
//
//
//}
