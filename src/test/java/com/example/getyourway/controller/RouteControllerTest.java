package com.example.getyourway.controller;

import com.example.getyourway.payload.test.DrivingRoute;
import com.example.getyourway.payload.request.RouteRequest;
import com.example.getyourway.service.RouteService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@ExtendWith(SpringExtension.class)
public class RouteControllerTest {

    private MockMvc mockMvc;
    @InjectMocks
    private RouteController routeController;


    @Mock
    private RouteService routeService;


    @BeforeEach
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(routeController).build();
    }

    @Test
    public void requestRoute_With_RouteRequestObject_ShouldReturnJson() throws Exception {

        // Arrange
        RouteRequest mockRouteRequest = new RouteRequest("5", "6", "7", "8");
        List<DrivingRoute> routes = new ArrayList<>();
        routes.add(new DrivingRoute());
        routes.add(new DrivingRoute());

        when(routeService.getRoutes(mockRouteRequest)).thenReturn(routes);

        // Act & Assert
        this.mockMvc.perform(post("/route")
                .contentType("application/json"));
    }
}
