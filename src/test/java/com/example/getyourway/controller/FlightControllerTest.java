package com.example.getyourway.controller;

import com.example.getyourway.payload.request.AirportRequest;
import com.example.getyourway.payload.test.Flight;
import com.example.getyourway.service.AllAirports;
import com.example.getyourway.service.FlightService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@ExtendWith(SpringExtension.class)
public class FlightControllerTest {

    private MockMvc mockMvc;
    @InjectMocks
    private FlightController flightController;

    @Mock
    private FlightService flightService;

    @BeforeEach
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(flightController).build();
    }

    @Test
    public void getAirportsShouldReturnListOfAirports () throws Exception {
        AirportRequest mockRouteRequest = new AirportRequest("London", "Barcelona");
        AllAirports airports = new AllAirports();
        when(flightService.getAirports(mockRouteRequest)).thenReturn(airports);

        this.mockMvc.perform(post("/")
                .contentType("application/json"));
    }

    @Test
    public void callFlightsAPIShouldReturnListOfFlights () throws Exception {
        AllAirports allAirports = new AllAirports();
        List<Flight> flightList = new ArrayList<>();
        when(flightService.callFlightAPI(allAirports)).thenReturn(flightList);

        this.mockMvc.perform(post("/flights")
                .contentType("application/json"));
    }
}