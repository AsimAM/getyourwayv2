package com.example.getyourway.repositories;

import com.example.getyourway.entities.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class UserRepositoryTest {
    @Autowired UserRepository userRepository;

    @Test
    public void testUserRepository() {
        User us1 = new User("hello", "mate");
        userRepository.save(us1);
        assertThat(userRepository.findById(us1.getId())).isNotNull();
    }

}
