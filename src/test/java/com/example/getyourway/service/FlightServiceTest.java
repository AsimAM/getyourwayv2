package com.example.getyourway.service;

import com.example.getyourway.payload.test.Flight;
import com.example.getyourway.payload.request.AirportRequest;
import com.example.getyourway.payload.test.Airport;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FlightServiceTest {

    @InjectMocks
    FlightService flightService;

    @Mock
    RestTemplate restTemplate;

    @Test
    public void testAirportRequestService() throws JsonProcessingException, JSONException, URISyntaxException {
        AirportRequest airportRequest = new AirportRequest("London", "Barcelona");
        ResponseEntity<String> mockResponse = new ResponseEntity<String>(
                "{\"airports\":[{\"name\":\"London - All Airports\",\"city\":\"London\",\"iata\":\"LON\",\"latitude\":\"51.507351\",\"longitude\":\"-0.127758\",\"country\":{\"name\":\"United Kingdom\",\"iso\":\"GB\"},\"state\":{\"name\":\"London\",\"type\":\"County\"}},{\"name\":\"London Gatwick Airport\",\"city\":\"London\",\"iata\":\"LGW\",\"latitude\":\"51.153662\",\"longitude\":\"-0.182063\",\"country\":{\"name\":\"United Kingdom\",\"iso\":\"GB\"},\"state\":{\"name\":\"West Sussex\",\"type\":\"County\"}}],\"term\":\"london\",\"limit\":\"2\",\"size\":0,\"cached\":true,\"status\":true,\"statusCode\":200}\n",
                null,
                HttpStatus.OK
        );
        when(restTemplate.exchange(any(URI.class), any(HttpMethod.class), any(HttpEntity.class), eq(String.class))).thenReturn(mockResponse);
        List<Airport> result = flightService.getAirports(airportRequest).getDepartureAirports();
        assertThat(result).hasSize(2);

    }

    @Test
    public void testFlightRequestService() throws JsonProcessingException, JSONException, URISyntaxException {
        AllAirports flightRequest = new AllAirports();
        Airport london = new Airport();
        london.setAirportCodeName("LON");
        london.setAirportName("London - All Airports");
        Airport london2 = new Airport();
        london2.setAirportCodeName("LGW");
        london2.setAirportName("London Gatwick Airport");
        Airport barcelona = new Airport();
        barcelona.setAirportCodeName("BCN");
        barcelona.setAirportName("Barcelona Airport");
        Airport barcelona2 = new Airport();
        barcelona.setAirportCodeName("BLA");
        barcelona.setAirportName("General José Antonio Anzoátegui International Airport");
        flightRequest.getDepartureAirports().add(london);
        flightRequest.getDepartureAirports().add(london2);
        flightRequest.getArrivalAirports().add(barcelona);
        flightRequest.getArrivalAirports().add(barcelona2);
        String mockResponse = "{\"search_id\":\"70e917aa-c816-43d2-9a4c-b57ae274dbd2\",\"data\":[{\"id\":\"032d01af488000004c027cc3_0\",\"dTime\":1603641000,\"dTimeUTC\":1603641000,\"aTime\":1603652700,\"aTimeUTC\":1603649100,\"nightsInDest\":null,\"duration\":{\"departure\":8100,\"return\":0,\"total\":8100},\"fly_duration\":\"2h 15m\",\"flyFrom\":\"STN\",\"cityFrom\":\"London\",\"cityCodeFrom\":\"LON\",\"countryFrom\":{\"code\":\"GB\",\"name\":\"United Kingdom\"},\"mapIdfrom\":\"london_gb\",\"flyTo\":\"BCN\",\"cityTo\":\"Barcelona\",\"cityCodeTo\":\"BCN\",\"countryTo\":{\"code\":\"ES\",\"name\":\"Spain\"},\"mapIdto\":\"barcelona_es\",\"distance\":1186.93,\"routes\":[[\"STN\",\"BCN\"]],\"airlines\":[\"FR\"],\"pnr_count\":1,\"has_airport_change\":false,\"technical_stops\":0,\"price\":8,\"bags_price\":{\"1\":41.4,\"2\":82.78},\"baglimit\":{\"hand_width\":40,\"hand_height\":20,\"hand_length\":55,\"hand_weight\":10,\"hold_width\":119,\"hold_height\":81,\"hold_length\":119,\"hold_dimensions_sum\":319,\"hold_weight\":20},\"availability\":{\"seats\":7},\"facilitated_booking_available\":false,\"conversion\":{\"EUR\":8},\"quality\":37.999925000000005,\"booking_token\":\"APYBK1pDMrvCrlhzGXYmXSK88Q1T0UpcQPOdqAZY4OYtbLW8a_tkpecc5SepBVTgvIvpez50_VbfUiueAVZfiIHsNaJrKtYVtLh2JMsg5MOz51fLRJC9SKsnSYAPaxj89_phQ-_PW0pSwU-HDnF5Pj48ZfwjsJ0QcxML0pQ0lJx-h8eybSuJ3qHfh8fmZyDL3Klum2nz8wxBXUkaNgZvfrwdkfq3qj2J47oAaJhxC1UsPNdkS_MIa6TwWBwos53jMxCzxFlRtA91nXl5I20JZaTIni6w_9hdGVhZM4eOue-YOsS1ezApYLuaVbja238ulnS5WNoMWShgifcP9WDF09SQWaF2wEXz4ENE2GlpYrWkrWx-OYF1KX1msPKqnSPzeS1zImg7p5RXEgMKqy6QWbEeeTN2MRL8x77VkNzPd-krV9FwTBLnxmxysNAHvK75vwTOci3m5vdL7SWLlLi8GGd1IZys8HYaLzwNqoVfjq3JVvetCCVeTEM3dQAMiIy-fUcpA_yvbh8A530YbtaAE36DfBZmMEQYHDfVJHY_IOdBvymMplYFJiHppE_mc5a67JTyS2C3-QcukJHv31o75Gw4c0IvkjV-GtOz-zlNrxWbduTefRVzp4xHS6R6Jb8zLhxEBa8JJHV7ekc1-aeG_KCEqxZ7R_PGKxYrdnJNho7g=\",\"deep_link\":\"https:\\/\\/www.kiwi.com\\/deep?from=STN&to=BCN&departure=25-10-2020&flightsId=032d01af488000004c027cc3_0&price=8&passengers=1&affilid=picky&lang=en&currency=EUR&booking_token=APYBK1pDMrvCrlhzGXYmXSK88Q1T0UpcQPOdqAZY4OYtbLW8a_tkpecc5SepBVTgvIvpez50_VbfUiueAVZfiIHsNaJrKtYVtLh2JMsg5MOz51fLRJC9SKsnSYAPaxj89_phQ-_PW0pSwU-HDnF5Pj48ZfwjsJ0QcxML0pQ0lJx-h8eybSuJ3qHfh8fmZyDL3Klum2nz8wxBXUkaNgZvfrwdkfq3qj2J47oAaJhxC1UsPNdkS_MIa6TwWBwos53jMxCzxFlRtA91nXl5I20JZaTIni6w_9hdGVhZM4eOue-YOsS1ezApYLuaVbja238ulnS5WNoMWShgifcP9WDF09SQWaF2wEXz4ENE2GlpYrWkrWx-OYF1KX1msPKqnSPzeS1zImg7p5RXEgMKqy6QWbEeeTN2MRL8x77VkNzPd-krV9FwTBLnxmxysNAHvK75vwTOci3m5vdL7SWLlLi8GGd1IZys8HYaLzwNqoVfjq3JVvetCCVeTEM3dQAMiIy-fUcpA_yvbh8A530YbtaAE36DfBZmMEQYHDfVJHY_IOdBvymMplYFJiHppE_mc5a67JTyS2C3-QcukJHv31o75Gw4c0IvkjV-GtOz-zlNrxWbduTefRVzp4xHS6R6Jb8zLhxEBa8JJHV7ekc1-aeG_KCEqxZ7R_PGKxYrdnJNho7g=\",\"tracking_pixel\":null,\"p1\":1,\"p2\":1,\"p3\":1,\"transfers\":[],\"type_flights\":[\"deprecated\"],\"virtual_interlining\":false,\"route\":[{\"fare_basis\":\"\",\"fare_category\":\"M\",\"fare_classes\":\"\",\"price\":1,\"fare_family\":\"\",\"found_on\":\"deprecated\",\"last_seen\":1602709870,\"refresh_timestamp\":1602709870,\"source\":\"deprecated\",\"return\":0,\"bags_recheck_required\":false,\"guarantee\":false,\"id\":\"032d01af488000004c027cc3_0\",\"combination_id\":\"032d01af488000004c027cc3\",\"original_return\":0,\"aTime\":1603652700,\"dTime\":1603641000,\"aTimeUTC\":1603649100,\"dTimeUTC\":1603641000,\"mapIdfrom\":\"london_gb\",\"mapIdto\":\"barcelona_es\",\"cityTo\":\"Barcelona\",\"cityFrom\":\"London\",\"cityCodeFrom\":\"LON\",\"cityCodeTo\":\"BCN\",\"flyTo\":\"BCN\",\"flyFrom\":\"STN\",\"airline\":\"FR\",\"operating_carrier\":\"FR\",\"equipment\":null,\"latFrom\":51.885,\"lngFrom\":0.235,\"latTo\":41.2969444,\"lngTo\":2.07833333,\"flight_no\":9014,\"vehicle_type\":\"aircraft\",\"operating_flight_no\":\"9014\"}]}],\"connections\":[],\"time\":1,\"currency\":\"EUR\",\"currency_rate\":1.0,\"fx_rate\":1.0,\"refresh\":[],\"del\":2.697902,\"ref_tasks\":[],\"search_params\":{\"flyFrom_type\":\"airport\",\"to_type\":\"airport\",\"seats\":{\"passengers\":1,\"adults\":1,\"children\":0,\"infants\":0}},\"all_stopover_airports\":[],\"all_airlines\":[],\"_results\":30}\n";
        when(restTemplate.getForObject(any(URI.class), eq(String.class))).thenReturn(mockResponse);
        List<Flight> result = flightService.callFlightAPI(flightRequest);
        assertThat(ResponseEntity.ok().body(result).getStatusCodeValue()).isEqualTo(200);
    }
}
