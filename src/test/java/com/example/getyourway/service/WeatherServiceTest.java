//package com.example.getyourway.service;
//
//import com.example.getyourway.domain.WeatherConditions;
//import com.example.getyourway.payload.request.WeatherRequest;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import org.json.JSONException;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.client.RestTemplate;
//import java.net.URI;
//import java.net.URISyntaxException;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.mockito.ArgumentMatchers.*;
//import static org.mockito.Mockito.when;
//
//@ExtendWith(MockitoExtension.class)
//public class WeatherServiceTest {
//
//    @InjectMocks
//    WeatherService mockWeatherService;
//
//    @Mock
//    RestTemplate restTemplate;
//
//    @Test
//    public void callingGetWeather_Returns200OKStatusCode() throws JsonProcessingException, URISyntaxException, JSONException {
//        // Arrange
//        WeatherRequest mockRequest = new WeatherRequest("41.505432", "-40.023533");
//        String mockResponse = "{\"list\":[{\"dt\":1602784800,\"main\":{\"temp\":284.44,\"feels_like\":279.27,\"temp_min\":284.38,\"temp_max\":284.44,\"pressure\":1017,\"sea_level\":1017,\"grnd_level\":1016,\"humidity\":68,\"temp_kf\":0.06},\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01d\"}],\"clouds\":{\"all\":0},\"wind\":{\"speed\":5.95,\"deg\":320},\"visibility\":10000,\"pop\":0,\"sys\":{\"pod\":\"d\"},\"dt_txt\":\"2020-10-15 18:00:00\"}]}";
//        when(restTemplate.getForObject(any(URI.class), eq(String.class))).thenReturn(mockResponse);
//
//        // Act
//        WeatherConditions result = mockWeatherService.getWeather(mockRequest);
//
//        // Assert
//        assertThat(ResponseEntity.ok().body(result).getStatusCodeValue()).isEqualTo(200);
//    }
//
//
//}
