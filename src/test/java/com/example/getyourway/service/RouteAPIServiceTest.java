package com.example.getyourway.service;

import com.example.getyourway.payload.test.DrivingRoute;
import com.example.getyourway.payload.request.RouteRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RouteAPIServiceTest {

    @InjectMocks
    RouteService routeService;

    @Mock
    RestTemplate restTemplate;

    @Test
    public void testRouteRequestService() throws JsonProcessingException, URISyntaxException, JSONException {

        // Arrange
        RouteRequest mockRouteRequest = new RouteRequest("5", "6", "7", "8");
        String mockResponse = "{\"routes\":[{\"weight_name\":\"auto\",\"weight\":0,\"duration\":0,\"distance\":1,\"legs\":[{\"steps\":[{\"maneuver\":{\"type\":\"depart\",\"instruction\":\"Drive north.\",\"bearing_after\":0,\"bearing_before\":0,\"location\":[9.014907,8.965717]},\"intersections\":[{\"bearings\":[0],\"entry\":[true],\"mapbox_streets_v8\":{\"class\":\"street\"},\"is_urban\":false,\"admin_index\":0,\"out\":0,\"geometry_index\":0,\"location\":[9.014907,8.965717]}],\"weight\":0,\"duration\":0,\"distance\":1,\"name\":\"\",\"driving_side\":\"right\",\"mode\":\"driving\",\"geometry\":{\"coordinates\":[[9.014907,8.965717],[9.014907,8.965717]],\"type\":\"LineString\"}},{\"maneuver\":{\"type\":\"arrive\",\"instruction\":\"You have arrived at your destination.\",\"bearing_after\":0,\"bearing_before\":0,\"location\":[9.014907,8.965717]},\"intersections\":[{\"bearings\":[180],\"entry\":[true],\"in\":0,\"admin_index\":0,\"geometry_index\":1,\"location\":[9.014907,8.965717]}],\"weight\":0,\"duration\":0,\"distance\":0,\"name\":\"\",\"driving_side\":\"right\",\"mode\":\"driving\",\"geometry\":{\"coordinates\":[[9.014907,8.965717],[9.014907,8.965717]],\"type\":\"LineString\"}}],\"admins\":[{\"iso_3166_1_alpha3\":\"NGA\",\"iso_3166_1\":\"NG\"}],\"duration\":0,\"distance\":1,\"weight\":0,\"summary\":\"\"}],\"geometry\":{\"coordinates\":[[9.014907,8.965717],[9.014907,8.965717]],\"type\":\"LineString\"}}],\"waypoints\":[{\"distance\":4153.482,\"name\":\"\",\"location\":[9.014908,8.965717]},{\"distance\":4256.034,\"name\":\"\",\"location\":[9.014908,8.965717]}],\"code\":\"Ok\",\"uuid\":\"jZCFoDA5BlY1cQ9bdnyUUbqiJFcftAMdOawCJOEq1LISbZZCCI3oHw==\"}\n";

        when(restTemplate.getForObject(any(URI.class), eq(String.class))).thenReturn(mockResponse);

        // Act
        List<DrivingRoute> result = routeService.getRoutes(mockRouteRequest);

        // Assert
        assertThat(ResponseEntity.ok().body(result).getStatusCodeValue()).isEqualTo(200);
    }

}
